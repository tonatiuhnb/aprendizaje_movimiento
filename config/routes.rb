Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  resources 'provider_sales' do
    get 'find_coupon', on: :collection
    post 'apply_coupon', on: :collection
  end

  resources 'companies'
  resources 'businesses'
  devise_for 'users', controllers: { registrations: 'registrations' }

  root to: "home#index"

  resources 'dashboard' do
    get 'extend_profile_1', on: :collection
    patch 'update_profile', on: :collection
    get 'available_coupons', on: :collection
    post 'obtain_coupon', on: :collection
    get 'coupons_wallet', on: :collection
  end

  resources 'games', only: ['index', 'show', 'evaluate'] do
    post 'process_user_input', on: :collection
  end

  get 'user_root' => 'dashboard#index', as: 'user_root'
  get 'dashboard/extend_profile_1' => 'dashboard#extend_profile_1'
  patch 'dashboard/update_profile' => 'dashboard#update_profile'
end
