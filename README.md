# README

Key name of the project "Aprendizaje en Movimiento"
App developed during the 2020 hackaton of https://www.hackcolima.com/

Aprendizaje en Movimiento aims to empower and motivate users who are in quarentine during and after the COVID19 contingency, to play with physical and mental games to get discounts on food and services.

* Instructions to setup the app

On Mac / Linux

```bash
# install RVM (the Ruby versions manager)
$ \curl -sSL https://get.rvm.io | bash
# install the required Ruby version
$ rvm install 2.6.5
# install the Ruby gems the app needs
$ bundle install
# run the app
$ rails server
```

On Windows

... To Be Defined


* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
