module ProviderSalesHelper
  def discount_types_for_selector
    ProviderSale::DISCOUNT_TYPES
      .length
      .times
      .map do |i|
        [I18n.t("discount_types.#{ProviderSale::DISCOUNT_TYPES[i]}"), i]
      end
  end

  def discount_to_ui(provider_sale)
    ProviderSale::DISCOUNT_TYPES[provider_sale['discount_type']] == 'percentage' ?
      "#{currency_db_to_ui_format(provider_sale['discount_amount'])}%" :
      "$#{currency_db_to_ui_format(provider_sale['discount_amount'])}"
  end
end
