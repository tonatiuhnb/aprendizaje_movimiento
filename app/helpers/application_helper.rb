module ApplicationHelper
  PAGES_WITH_PUZZLE_BACKGROUND = %w(
    RegistrationsController#new
    DashboardController#extend_profile_1
    DashboardController#index
    DashboardController#available_coupons
    DashboardController#coupons_wallet
    SessionsController#new
    Devise::SessionsController#new
    ProviderSalesController#new
    ProviderSalesController#index
    ProviderSalesController#edit
    ProviderSalesController#edit
    ProviderSalesController#find_coupon
    GamesController#index
    GamesController#show
  )

  PAGES_WITH_TOP_GRAPH = %w(
    HomeController#index
  )

  PAGES_WITH_GENERIC_LOGO = %w(
    DashboardController#extend_profile_1
    DashboardController#index
    DashboardController#available_coupons
    DashboardController#coupons_wallet
    Devise::SessionsController#new
    ProviderSalesController#new
    ProviderSalesController#index
    ProviderSalesController#edit
    ProviderSalesController#find_coupon
    GamesController#index
    GamesController#show
  )

  TOP_GRAPH_SIZES = {
    'DashboardController#extend_profile_1' => '55%',
    'DashboardController#index' => '75%',
    'GamesController#index' => '45%',
    'Devise::SessionsController#new' => '50%',
    'ProviderSalesController#new' => '45%',
    'ProviderSalesController#index' => '40%'
  }

  TOP_GRAPH_SIZES.default = '100%'

  def add_puzzle_class
    PAGES_WITH_PUZZLE_BACKGROUND
      .include?(controller_action_identifier) ?
      'puzzle_background' :
      ''
  end

  def include_top_graph?
    PAGES_WITH_TOP_GRAPH.include?(controller_action_identifier)
  end

  def include_generic_logo?
    PAGES_WITH_GENERIC_LOGO.include?(controller_action_identifier)
  end

  def controller_action_identifier
    "#{controller.class.to_s}##{controller.action_name}"
  end

  def top_graph_size
    TOP_GRAPH_SIZES[controller_action_identifier]
  end

  def perform_operation_label(element_id, label_for_update, label_for_create)
    element_id.present? ? label_for_update : label_for_create
  end

  def currency_ui_to_db_format(currency_amount)
    (currency_amount.to_f * 100).to_i
  end

  def currency_db_to_ui_format(currency_amount, opts = {})
    return currency_amount if currency_amount.to_f <= 0
    currency_amount.to_f / 100
  end
end
