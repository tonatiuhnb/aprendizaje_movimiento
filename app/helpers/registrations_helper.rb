module RegistrationsHelper
  def company_categories_for_selector
    Company::COMPANY_TYPES
      .length
      .times
      .map do |i|
        [I18n.t("company_categories.#{Company::COMPANY_TYPES[i]}"), i]
      end
  end

  def country_states_for_selector
    Company::MEXICO_STATES
      .keys
      .map do |state_code|
        [Company::MEXICO_STATES[state_code], state_code]
      end
  end
end
