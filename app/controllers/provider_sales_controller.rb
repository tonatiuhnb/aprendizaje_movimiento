class ProviderSalesController < ApplicationController
  load_and_authorize_resource

  before_action :authenticate_user!

  def index
    @coupons = ProviderCouponsDecorator
      .new(ProviderSale.where(provider_id: current_user.id))
      .decorate
  end

  def new
  end

  def edit
    @provider_sale.discount_amount = helpers.currency_db_to_ui_format(@provider_sale.discount_amount)
    @provider_sale.product_price = helpers.currency_db_to_ui_format(@provider_sale.product_price)
  end

  def create
    @provider_sale = ProviderSale.new(
      provider_sale_params.merge(
        discount_amount: helpers.currency_ui_to_db_format(params[:provider_sale][:discount_amount]),
        product_price: helpers.currency_ui_to_db_format(params[:provider_sale][:product_price])
      )
    )

    respond_to do |format|
      if @provider_sale.save
        format.html { redirect_to edit_provider_sale_path(@provider_sale), notice: 'Cupón creado.' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    params = provider_sale_params.merge(
      discount_amount: helpers.currency_ui_to_db_format(provider_sale_params[:discount_amount]),
      product_price: helpers.currency_ui_to_db_format(provider_sale_params[:product_price])
    )

    respond_to do |format|
      if @provider_sale.update(params)
        format.html { redirect_to provider_sales_path, notice: 'Cupón actualizado.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @provider_sale.destroy
    respond_to do |format|
      format.html { redirect_to provider_sales_url, notice: 'Provider sale was successfully destroyed.' }
    end
  end

  def find_coupon; end

  def apply_coupon
    @consumer_coupon = ConsumerCoupon
      .where(
        provider_coupon_id: ProviderSale
          .where(provider_id: current_user.id)
          .pluck(:id)
      )
      .where('coupon_applied IS NOT true')
      .where(code: params[:coupon_code])
      .first

    unless @consumer_coupon
      flash[:alert] = 'No se encontró ningún cupon activo con ese código. Por favor verifica el código.'
      return redirect_to action: :find_coupon
    end

    if @consumer_coupon.provider_coupon.available_stock < 1
      flash[:alert] = 'Hay un problema de stock con el cupón.'
      return redirect_to action: :find_coupon
    end

    @consumer_coupon.update_attributes!(coupon_applied: true)
    flash[:notice] = 'El cupón ha sido aplicado!'

    redirect_to action: :find_coupon
  end

  private
    def provider_sale_params
      params
        .require(:provider_sale)
        .permit(:provider_id, :title, :description, :product_price, :discount_type, :discount_amount, :required_credits, :available_stock, :reference_image)
    end
end
