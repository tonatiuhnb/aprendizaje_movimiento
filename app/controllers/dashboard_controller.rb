class DashboardController < ApplicationController
  before_action :authenticate_user!
  # before_action :load_and_authorize_resource

  def index
  end

  # NOTE: not in use right now
  def extend_profile_1
  end

  def update_profile
    current_user.update_attributes(profile_params)
    redirect_to action: :index
  end

  def available_coupons
    @coupons = ProviderCouponsDecorator
      .new(
        ProviderSale
          .all
          .order(id: 'desc')
          .where('available_stock > 0')
        )
      .decorate
  end

  def obtain_coupon
    coupon_obtainer = CouponObtainer.new(
      consumer: current_user,
      provider_coupon: ProviderSale.find(params[:coupon_id])
    )
    coupon_obtainer.perform

    if coupon_obtainer.successful?
      flash[:notice] = 'Cupón obtenido. El cupón ha sido agregado a tu cartera de cupones.'
      redirect_to action: :coupons_wallet
    else
      flash[:alert] = "Ha ocurrido un error. Intenta más tarde. Código(s) de error: #{coupon_obtainer.error_codes}"
      redirect_to action: :available_coupons
    end
  end

  def coupons_wallet
    @coupons = ConsumerCouponsDecorator
      .new(
        ConsumerCoupon.includes(:provider_coupon)
                      .where(user_id: current_user.id)
                      .order('id DESC')
      )
      .decorate


    if @coupons.length < 1
      flash[:alert] = 'No haz agregado ningún ticket a tu cartera aún. Juega y gana cupones!'
      redirect_to action: 'index'
    end
  end

  private

  def profile_params
    params
      .require('user')
      .permit(:avatar_id)
  end
end
