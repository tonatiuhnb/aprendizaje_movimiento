class RegistrationsController < Devise::RegistrationsController
  def new
    build_resource({})
    self.resource.company = Company.new()
    respond_with self.resource
  end

  def create
    super
  end

  private

  def sign_up_params
    params
      .require(resource_name)
      .permit(
        :full_name,
        :email,
        :password,
        :password_confirmation,
        :consumer_role,
        :provider_role,
        :age,
        [
          company_attributes: [
            :name,
            :address,
            :category_id,
            :offers_physical_products,
            :offers_digital_products,
            :offers_credit_card_payments,
            :offers_cash_payments,
            :owner_full_name,
            :phone_number,
            :street_address,
            :address_state_id,
            :address_zip_code,
          ]
        ]
      )
  end
end
