class HomeController < ApplicationController
  def index
    return redirect_to rails_admin_path if current_user.try(:admin_role?)
    return redirect_to(controller: :dashboard, action: :index) if user_signed_in?
  end
end
