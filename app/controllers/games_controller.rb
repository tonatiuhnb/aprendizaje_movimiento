class GamesController < ApplicationController
  def index
    @games = Game.all.where(visible: true)
  end

  def show
    @game = Game.find(params[:id])
  end

  def process_user_input
    @game = Game.find(params[:id])
    @results = params[:yes_no_question_results]
      .keys
      .reduce({}) do |acc, yes_no_question_id|
        yes_no_question_result_answer = params[:yes_no_question_results][yes_no_question_id] == 'on'
        yes_no_question = YesNoQuestion.find(yes_no_question_id)

        acc[yes_no_question_id] = yes_no_question.is_true == yes_no_question_result_answer ? 'correct' : 'incorrect'
        acc
      end

    if @results.values.include?('incorrect')
      flash[:alert] = "Algunas de tus respuestas fueron incorrectas, intenta de nuevo!"
    else
      flash[:notice] = "Contestaste la trivia correctamente, haz ganado #{@game.points_offered} puntos!"

      current_user.update_attributes(
        points_earned: current_user.points_earned + @game.points_offered
      )
    end

    redirect_to game_path(id: @game.id)
  end
end
