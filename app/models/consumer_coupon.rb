class ConsumerCoupon < ActiveRecord::Base
  belongs_to :provider_coupon, class_name: 'ProviderSale'
end
