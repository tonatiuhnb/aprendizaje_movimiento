class Quizz < Game
  RailsAdmin.config do |config|
    config.model 'Quizz' do
      edit do
        field :title
        field :description
        field :summary
        field :visible
        field :game_image
        field :points_offered

        field :type do
          read_only true
        end
      end
    end
  end
end
