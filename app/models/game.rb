class Game < ApplicationRecord
  has_many :yes_no_questions, foreign_key: 'game_id'

  has_one_attached :game_image
end
