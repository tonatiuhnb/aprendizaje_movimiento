# TODO: rename to `provider_sale`
class ProviderSale < ApplicationRecord
  # NOTE: records in the Database depend on the order of `DISCOUNT_TYPES`
  DISCOUNT_TYPES = %w[
    percentage
    money_amount
  ];

  has_one_attached :reference_image
  # has_one :consumer_coupon, foreign_key: 'provider_coupon_id'
end
