class YesNoQuestion < ApplicationRecord
  belongs_to :game

  has_one_attached :yes_no_question_image
end
