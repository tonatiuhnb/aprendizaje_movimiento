class Company < ApplicationRecord
  # NOTE: records in the database depend on the order of this array (don't
  # change it unless you're going to make the corresponding updates on the database)
  COMPANY_TYPES = [
    :food_and_drinks,
    :home_products,
    :clothe,
    :groceries
  ].freeze

  MEXICO_STATES = {
    "CDMX" => "CDMX",
    "AGS" => "Aguascalientes",
    "BCN" => "Baja California",
    "BCS" => "Baja California Sur",
    "CAM" => "Campeche",
    "CHP" => "Chiapas",
    "CHI" => "Chihuahua",
    "COA" => "Coahuila",
    "COL" => "Colima",
    "DUR" => "Durango",
    "GTO" => "Guanajuato",
    "GRO" => "Guerrero",
    "HGO" => "Hidalgo",
    "JAL" => "Jalisco",
    "MEX" => "M&eacute;xico",
    "MIC" => "Michoac&aacute;n",
    "MOR" => "Morelos",
    "NAY" => "Nayarit",
    "NLE" => "Nuevo Le&oacute;n",
    "OAX" => "Oaxaca",
    "PUE" => "Puebla",
    "QRO" => "Quer&eacute;taro",
    "ROO" => "Quintana Roo",
    "SLP" => "San Luis Potos&iacute;",
    "SIN" => "Sinaloa",
    "SON" => "Sonora",
    "TAB" => "Tabasco",
    "TAM" => "Tamaulipas",
    "TLX" => "Tlaxcala",
    "VER" => "Veracruz",
    "YUC" => "Yucat&aacute;n",
    "ZAC" => "Zacatecas"
  }.freeze

  validates :name, uniqueness: true
end
