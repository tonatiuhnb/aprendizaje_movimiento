class CouponObtainer
  attr_reader :consumer, :provider_coupon
  attr_accessor :error_codes

  def initialize(consumer:, provider_coupon:)
    @consumer = consumer
    @provider_coupon = provider_coupon
    @error_codes = []
  end

  def perform
    return unless validate_input_data

    ActiveRecord::Base.transaction do
      create_consumer_coupon!
      deduct_provider_coupon_stock!
      deduct_consumer_credits!
    end
  rescue StandardError => _e
    error_codes << '1'
  end

  def successful?
    error_codes.empty?
  end

  private

  def validate_input_data
    error_codes << 'consumer_has_not_enough_points' if consumer.points_earned < provider_coupon.required_credits
    error_codes << 'provider_coupon_hasnt_stock' if provider_coupon.available_stock < 1

    error_codes.blank?
  end

  def create_consumer_coupon!
    ConsumerCoupon.create!(
      user_id: consumer.id,
      provider_coupon_id: provider_coupon.id,
      code: random_code
    )
  end

  def deduct_provider_coupon_stock!
    provider_coupon.update_attributes!(
      available_stock: (provider_coupon.available_stock - 1)
    )
  end

  def deduct_consumer_credits!
    consumer.update_attributes!(
      points_earned: (consumer.points_earned - provider_coupon.required_credits)
    )
  end

  def random_code
    (0...4).map { (65 + rand(26)).chr }.join
  end
end
