class ConsumerCouponsDecorator
  attr_reader :consumer_coupons

  def initialize(consumer_coupons)
    @consumer_coupons = consumer_coupons
  end

  def decorate
    consumer_coupons.map do |consumer_coupon|
      consumer_coupon
        .attributes
        .merge(
          type: 'consumer_coupon'
        )
        .merge(
          consumer_coupon
            .provider_coupon
            .attributes
        )
        .merge(
          reference_image: consumer_coupon.provider_coupon.reference_image
        )
        .with_indifferent_access
    end
  end
end
