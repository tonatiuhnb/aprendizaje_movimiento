class ProviderCouponsDecorator
  attr_reader :provider_coupons

  def initialize(provider_coupons)
    @provider_coupons = provider_coupons
  end

  def decorate
    provider_coupons.map do |provider_coupon|
      provider_coupon
        .attributes
        .merge(
          reference_image: provider_coupon.reference_image,
          type: 'provider_coupon'
        )
        .with_indifferent_access
    end
  end
end
