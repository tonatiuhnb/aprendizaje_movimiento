class AddMoreFieldsToCompanyRoundTwo < ActiveRecord::Migration[6.0]
  def change
    add_column :companies, :street_address, :string
    add_column :companies, :address_state_code, :string
    add_column :companies, :address_zip_code, :string

    remove_column :companies, :address, :string
  end
end
