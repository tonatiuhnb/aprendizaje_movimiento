class CreateProviderSales < ActiveRecord::Migration[6.0]
  def change
    create_table :provider_sales do |t|
      t.integer :provider_id
      t.integer :discount_percentage
      t.integer :product_price
      t.integer :required_credits
      t.integer :available_stock

      t.timestamps
    end
  end
end
