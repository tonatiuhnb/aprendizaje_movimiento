class AddDiscountTypeToProviderSales < ActiveRecord::Migration[6.0]
  def change
    add_column :provider_sales, :discount_type, :integer
    add_column :provider_sales, :discount_amount, :integer
    remove_column :provider_sales, :discount_percentage, :integer
  end
end
