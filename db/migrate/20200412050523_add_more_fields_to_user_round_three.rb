class AddMoreFieldsToUserRoundThree < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :age, :integer
    add_column :users, :full_name, :string

    remove_column :users, :user_name, :string
  end
end
