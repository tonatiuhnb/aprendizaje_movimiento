class CreateYesNoQuestions < ActiveRecord::Migration[6.0]
  def change
    create_table :yes_no_questions do |t|
      t.string :title
      t.boolean :is_true
      t.references :game, null: false, foreign_key: true

      t.timestamps
    end
  end
end
