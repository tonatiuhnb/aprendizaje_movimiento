class AddFieldToConsumerCoupon < ActiveRecord::Migration[6.0]
  def change
    add_column :consumer_coupons, :coupon_applied, :boolean, default: false
  end
end
