class ChangeGameType < ActiveRecord::Migration[6.0]
  def change
    change_column :games, :type, :string
  end
end
