class AddProductNameToProviderSales < ActiveRecord::Migration[6.0]
  def change
    add_column :provider_sales, :product_name, :string
  end
end
