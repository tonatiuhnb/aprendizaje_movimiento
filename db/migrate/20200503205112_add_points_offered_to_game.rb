class AddPointsOfferedToGame < ActiveRecord::Migration[6.0]
  def change
    add_column :games, :points_offered, :integer
  end
end
