class AddPointsEarnedToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :points_earned, :integer, default: 0
  end
end
