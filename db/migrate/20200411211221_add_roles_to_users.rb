class AddRolesToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :consumer_role, :boolean
    add_column :users, :provider_role, :boolean
  end
end
