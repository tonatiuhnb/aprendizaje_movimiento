class AddDescriptionToGame < ActiveRecord::Migration[6.0]
  def change
    add_column :games, :description, :string
    add_column :games, :summary, :text
  end
end
