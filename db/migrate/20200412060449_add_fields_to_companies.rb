class AddFieldsToCompanies < ActiveRecord::Migration[6.0]
  def change
    add_column :companies, :offers_credit_card_payments, :boolean
    add_column :companies, :offers_cash_payments, :boolean
    add_column :companies, :owner_full_name, :string
    add_column :companies, :phone_number, :string
  end
end
