class CreateCompanies < ActiveRecord::Migration[6.0]
  def change
    create_table :companies do |t|
      t.string :name
      t.string :address
      t.integer :category_id
      t.boolean :offers_physical_products
      t.boolean :offers_digital_products

      t.timestamps
    end
  end
end
