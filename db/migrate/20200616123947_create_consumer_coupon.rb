class CreateConsumerCoupon < ActiveRecord::Migration[6.0]
  def change
    create_table :consumer_coupons do |t|
      t.integer :user_id
      t.integer :provider_coupon_id
      t.string :code
    end
  end
end
