class AddDescriptionToProviderSales < ActiveRecord::Migration[6.0]
  def change
    add_column :provider_sales, :title, :string
    add_column :provider_sales, :description, :string
    remove_column :provider_sales, :product_name, :string
  end
end
